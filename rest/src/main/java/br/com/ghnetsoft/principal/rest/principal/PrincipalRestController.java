package br.com.ghnetsoft.principal.rest.principal;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ASC;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.REGISTROS_POR_PAGINA_PADRAO;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.MappedSuperclass;

import br.com.ghnetsoft.principal.core.resorce.PaginacaoEnvioResource;
import br.com.ghnetsoft.principal.core.resorce.PrincipalEnvioResource;
import br.com.ghnetsoft.principal.rest.mensagens.MensagensRetorno;
import lombok.AllArgsConstructor;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@AllArgsConstructor(access = PROTECTED)
public abstract class PrincipalRestController extends MensagensRetorno {

	private static final long serialVersionUID = 4573806151452657708L;

	/**
	 * Envio de filtro padrão
	 * 
	 * @param filtro
	 */
	protected void envioFiltro(PaginacaoEnvioResource filtro) {
		if (filtro.getDirecao() == null) {
			filtro.setDirecao(ASC);
		}
		if (filtro.getPaginaAtual() == null) {
			filtro.setPaginaAtual(0);
		}
		if (filtro.getQuantidadeRegistros() == null) {
			filtro.setQuantidadeRegistros(REGISTROS_POR_PAGINA_PADRAO);
		}
	}

	/**
	 * preenchimento de usuário logado
	 * 
	 * @param username
	 * @return
	 */
	protected PrincipalEnvioResource preencherEnvioResource(String username) {
		PrincipalEnvioResource resource = new PrincipalEnvioResource();
		resource.setLoginUsuario(username);
		return resource;
	}
}
