package br.com.ghnetsoft.principal.rest.mensagens;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ERRO_GERAL;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.EXCLUIDO_COM_SUCESSO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.REGISTRO_EXCLUIDO_COM_SUCESSO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.SALVAR_COM_ERRO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.SALVAR_COM_SUCESSO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.SALVO_COM_ERRO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.SALVO_COM_SUCESSO;
import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.ERROR;
import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.SUCCESS;
import static org.springframework.http.HttpStatus.CONFLICT;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.ResponseEntity;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum;
import lombok.extern.apachecommons.CommonsLog;

/**
 * @author Guilherme C Lopes
 */
@CommonsLog
public abstract class MensagensRetorno implements Serializable {

	private static final long serialVersionUID = 6483627645216815451L;

	protected static final String MENSAGEM_GERAL = "Favor entrar em contato com o administrador !";

	/**
	 * mensagem padrão de retorno ao salvar
	 * 
	 * @param erro
	 * @return
	 */
	public Collection<Mensagem> mensagemSalvar(boolean erro) {
		Collection<Mensagem> mensagens = new ArrayList<>();
		if (erro) {
			mensagens.add(new Mensagem(ERROR, SALVO_COM_ERRO, SALVAR_COM_ERRO));
		} else {
			mensagens.add(new Mensagem(SUCCESS, SALVO_COM_SUCESSO, SALVAR_COM_SUCESSO));
		}
		return mensagens;
	}

	/**
	 * Mensagem padrão de excluir
	 * 
	 * @return
	 */
	public Collection<Mensagem> mensagemExcluir() {
		Collection<Mensagem> mensagens = new ArrayList<>();
		mensagens.add(new Mensagem(SUCCESS, EXCLUIDO_COM_SUCESSO, REGISTRO_EXCLUIDO_COM_SUCESSO));
		return mensagens;
	}

	/**
	 * Mensagem de exceção por entidade
	 * 
	 * @param e
	 * @param mensagens
	 * @return
	 */
	public ResponseEntity<?> excecaoPorEntidade(RuntimeException e, Collection<Mensagem> mensagens) {
		log.error(e.getMessage(), e);
		Collection<Mensagem> mensagems = mensagemSalvar(true);
		mensagems.addAll(mensagens);
		return new ResponseEntity<>(mensagems, CONFLICT);
	}

	/**
	 * Mensagem de exceção geral sem ser por regra de negocio para salvar
	 * 
	 * @param e
	 * @param tipoErro
	 * @param chave
	 * @param mensagem
	 * @return
	 */
	public ResponseEntity<?> excecaoGeralSalvar(Exception e, TipoMensagemEnum tipoErro, String chave, String mensagem) {
		log.error(e.getMessage(), e);
		Collection<Mensagem> mensagens = new ArrayList<>();
		mensagens.add(new Mensagem(ERROR, SALVO_COM_ERRO, SALVAR_COM_ERRO));
		mensagens.add(new Mensagem(tipoErro, chave, mensagem));
		return new ResponseEntity<>(mensagens, CONFLICT);
	}

	/**
	 * Retorno de exceção que não seja regra de negócio diferente de salvar
	 * 
	 * @param e
	 * @param chave
	 * @param mensagem
	 * @return
	 */
	public ResponseEntity<?> erroExceptionGeral(Exception e, String chave, String mensagem) {
		log.error(e.getMessage(), e);
		Collection<Mensagem> mensagens = new ArrayList<>();
		if (StringUtils.isNoneEmpty(mensagem)) {
			mensagens.add(new Mensagem(ERROR, chave + ERRO_GERAL, mensagem));
		}
		return new ResponseEntity<>(mensagens, CONFLICT);
	}
}
