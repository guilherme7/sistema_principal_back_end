package br.com.ghnetsoft.principal.utilitario.stringUtils;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.BARRA;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.CARACTERES_DIGITOS;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.PARENTESES_DIREITO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.PARENTESES_ESQUERDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.REGEX_PONTO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.REMOVE_ACENTO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.TRACO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.VAZIO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ZERO;
import static java.text.Normalizer.normalize;
import static java.text.Normalizer.Form.NFD;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class StringUtilsGH {

	/**
	 * retira os caracteres da string, para deixar somente numeros
	 * 
	 * @param documento
	 * @return
	 */
	public static String replaceAllDocumento(String documento) {
		if (isNotEmpty(documento)) {
			return documento.replaceAll(REGEX_PONTO, VAZIO).replaceAll(TRACO, VAZIO).replaceAll(BARRA, VAZIO)
					.replaceAll(VAZIO, VAZIO);
		}
		return null;
	}

	/**
	 * remove acentos
	 * 
	 * @param texto
	 * @return
	 */
	public static String removeAcento(String texto) {
		if (isNotEmpty(texto)) {
			texto = normalize(texto, NFD);
			return texto.replaceAll(REMOVE_ACENTO, VAZIO);
		}
		return null;
	}

	/**
	 * retorna somente numeros
	 * 
	 * @param str
	 * @return
	 */
	public static String retornaApenasDigitos(String str) {
		if (isNotEmpty(str)) {
			return str.replaceAll(CARACTERES_DIGITOS, VAZIO);
		}
		return null;
	}

	/**
	 * retorna numero com a mascara do cep
	 * 
	 * @param valor
	 * @return
	 */
	public static String mascararToCep(String valor) {
		if (isNotEmpty(valor)) {
			String valor2 = preencheStringComCaracter(valor.toString(), ZERO, 8);
			return valor2.substring(0, 5) + TRACO + valor2.substring(5, 8);
		}
		return null;
	}

	/**
	 * retorna numero com a mascara do telefone
	 * 
	 * @param valor
	 * @return
	 */
	public static String mascararToTelefone(String valor) {
		if (isNotEmpty(valor)) {
			String valor2 = preencheStringComCaracter(valor.toString(), ZERO, 10);
			return PARENTESES_DIREITO + valor2.substring(0, 2) + PARENTESES_ESQUERDO + valor2.substring(2, 6) + TRACO
					+ valor2.substring(6, 10);
		}
		return null;
	}

	/**
	 * retorna numero com a concatenacao
	 * 
	 * @param texto
	 * @param caracter
	 * @param tamanho
	 * @return
	 */
	public static String preencheStringComCaracter(String texto, String caracter, Integer tamanho) {
		if (isNotEmpty(texto)) {
			String textoRetorno = VAZIO;
			Integer contador = 0;
			if (texto.length() < tamanho) {
				for (contador = 1; contador <= (tamanho - texto.length()); contador++) {
					textoRetorno += caracter;
				}
				return (textoRetorno + texto);
			} else {
				return texto;
			}
		}
		return null;
	}

	/**
	 * Troca tudo em uma string, informando o de e o para
	 * 
	 * @param valor
	 * @param trocaDe
	 * @param trocaPara
	 * @return
	 */
	public static String trocarString(String valor, String trocaDe, String trocaPara) {
		return valor.replaceAll(trocaDe, trocaPara);
	}
}
