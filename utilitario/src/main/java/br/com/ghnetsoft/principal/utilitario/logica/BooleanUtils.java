package br.com.ghnetsoft.principal.utilitario.logica;

import static java.lang.Boolean.valueOf;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class BooleanUtils {

	private static final String STRING_FALSE = "false";
	private static final String STRING_TRUE = "true";

	/**
	 * validação and
	 * 
	 * @param array
	 * @return
	 */
	public static boolean and(boolean... array) {
		return org.apache.commons.lang3.BooleanUtils.and(array);
	}

	/**
	 * validação or
	 * 
	 * @param array
	 * @return
	 */
	public static boolean or(boolean... array) {
		return org.apache.commons.lang3.BooleanUtils.or(array);
	}

	/***
	 * retorna 1 ou 0
	 * 
	 * @param valor
	 * @return
	 */
	public static String conversorBooleanoZeroUm(boolean valor) {
		return valor ? "1" : "0";
	}

	/**
	 * Retorna sim ou não
	 * 
	 * @param valor
	 * @return
	 */
	public static String conversorBooleanoSimNao(boolean valor) {
		return valor ? "Sim" : "Não";
	}

	/**
	 * Conversão de um string em booleman
	 * 
	 * @param valor
	 * @return
	 */
	public static String conversorValoresBooleano(String valor) {
		return isBoolean(valor) ? conversorBooleanoZeroUm(valueOf(valor)) : valor;
	}

	/**
	 * valida para ver se o valor é boolean
	 * 
	 * @param valor
	 * @return
	 */
	public static boolean isBoolean(String valor) {
		return STRING_TRUE.equals(valor) || STRING_FALSE.equals(valor);
	}
}
