package br.com.ghnetsoft.principal.utilitario.calendario;

import static br.com.ghnetsoft.principal.utilitario.logica.BooleanUtils.and;
import static br.com.ghnetsoft.principal.utilitario.logica.BooleanUtils.or;
import static org.joda.time.Days.daysBetween;
import static org.joda.time.LocalDate.parse;
import static org.joda.time.format.DateTimeFormat.forPattern;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;
import org.joda.time.LocalDate;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class DateUtil {

	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String MM_YYYY = "MM/yyyy";
	public static final String YYYYMM = "YYYYMM";
	public static final String YYYYMMDD = "YYYYMMdd";
	public static final String YYYY_MM = "YYYY-MM";
	public static final String YYYY_MM_DD = "YYYY-MM-dd";
	private static final int FIM_MES = 6;
	private static final int INICIO_MES = 4;
	private static final int TAMANHO_ANO_MES = 7;

	public static String converterAnoMesDiaEmMesAno(String data) {
		String dataAux = data;
		String sinalDivisao = "/";
		String sinalMenor = "-";
		if (!StringUtils.isBlank(data) && data.contains(sinalMenor)) {
			dataAux = StringUtils.remove(data, sinalMenor);
			dataAux = dataAux.substring(INICIO_MES, FIM_MES) + sinalDivisao + dataAux.substring(0, INICIO_MES);
		}
		return dataAux;
	}

	/**
	 * Retorna a data no formato Date
	 * 
	 * @param data
	 * @return
	 */
	public static Date converterJavaDate(LocalDate data) {
		return data.toDate();
	}

	/**
	 * Retorna a data no formato de String com a mascara que deseja
	 * 
	 * @param date
	 * @param pattern
	 * @return
	 */
	public static String converterLocalDateParaString(LocalDate date, String pattern) {
		return date.toString(pattern);
	}

	/**
	 * Retorna a data no formato LocalDate
	 * 
	 * @param strDate
	 * @param pattern
	 * @return
	 */
	public static LocalDate converterStringParaLocalDate(String strDate, String pattern) {
		return parse(strDate, forPattern(pattern));
	}

	/**
	 * Retorna se a data passada no 3º parametro estrá entre as datas passadas por
	 * parametros 1 e 2
	 * 
	 * @param startDate
	 * @param endDate
	 * @param checkDate
	 * @return
	 */
	public static Boolean dataNoIntervalo(LocalDate startDate, LocalDate endDate, LocalDate checkDate) {
		return or(startDate == null, endDate == null)
				|| and(!checkDate.isBefore(startDate), !checkDate.isAfter(endDate));
	}

	/**
	 * Retorna a diferenca de dias entre as datas passadas
	 * 
	 * @param dataInicial
	 * @param dataFinal
	 * @return
	 */
	public static Integer diferencaEmDias(LocalDate dataInicial, LocalDate dataFinal) {
		return daysBetween(dataInicial, dataFinal).getDays();
	}

	/**
	 * Retorna e valida se as datas são iguais
	 * 
	 * @param strDate1
	 * @param strDate2
	 * @return
	 */
	public static boolean isDataIgualIgnoraPattern(String strDate1, String strDate2) {
		LocalDate localDate1;
		LocalDate localDate2;
		boolean isEqual;
		if (isDiario(strDate1, TAMANHO_ANO_MES)) {
			localDate1 = converterStringParaLocalDate(strDate1, DD_MM_YYYY);
			localDate2 = converterStringParaLocalDate(strDate2, YYYY_MM_DD);
			isEqual = localDate1.equals(localDate2);
		} else {
			localDate1 = converterStringParaLocalDate(strDate1, MM_YYYY);
			localDate2 = converterStringParaLocalDate(strDate2, YYYY_MM);
			isEqual = localDate1.equals(localDate2);
		}
		return isEqual;
	}

	/**
	 * retorna se a data passada com parametro tem o tamanho definido no segundo
	 * parametro
	 * 
	 * @param strDate
	 * @return
	 */
	public boolean isDiario(String strDate, Integer tamanho) {
		return strDate.length() > tamanho;// TAMANHO_ANO_MES
	}

	/**
	 * verifica se as datas passadas como parametro estão no mesmo mês
	 * 
	 * @param dataBaseInclusao
	 * @param dataAtual
	 * @return
	 */
	public static boolean isMesCorrente(LocalDate dataBaseInclusao, LocalDate dataAtual) {
		return converterLocalDateParaString(dataBaseInclusao, MM_YYYY)
				.equals(converterLocalDateParaString(dataAtual, MM_YYYY));
	}

	/**
	 * Retorna a data do primeiro dia do mês
	 * 
	 * @param mesAno
	 * @param pattern
	 * @return
	 */
	public static LocalDate obterDataComPrimeiroDiaMes(String mesAno, String pattern) {
		LocalDate data = parse(mesAno, forPattern(pattern));
		return data.dayOfMonth().withMinimumValue();
	}

	/**
	 * Retorna o primeiro dia do mês
	 * 
	 * @param data
	 * @return
	 */
	public static LocalDate obterDataComPrimeiroDiaMes(LocalDate data) {
		return data.dayOfMonth().withMinimumValue();
	}

	/**
	 * Retorna o primeiro dia do mês seguinte
	 * 
	 * @param data
	 * @return
	 */
	public static LocalDate obterDataComPrimeiroDiaMesSeguinte(LocalDate data) {
		return data.plusMonths(1).dayOfMonth().withMinimumValue();
	}

	/**
	 * Retorna o último dia do mês
	 * 
	 * @param mesAno
	 * @param pattern
	 * @return
	 */
	public static LocalDate obterDataComUltimoDiaMes(String mesAno, String pattern) {
		LocalDate data = parse(mesAno, forPattern(pattern));
		return data.dayOfMonth().withMaximumValue();
	}

	/**
	 * Retorna o último dia do mês
	 * 
	 * @param data
	 * @return
	 */
	public static LocalDate obterDataComUltimoDiaMes(LocalDate data) {
		return data.dayOfMonth().withMaximumValue();
	}

	/**
	 * Retorna o ultimo dia do mês anterior
	 * 
	 * @param data
	 * @return
	 */
	public static LocalDate obterDataMesAnteriorComUltimoDiaMes(LocalDate data) {
		return data.minusMonths(1).dayOfMonth().withMaximumValue();
	}
}
