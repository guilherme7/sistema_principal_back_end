package br.com.ghnetsoft.principal.utilitario.validacao;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.EMAIL_PATTERN;
import static java.util.regex.Pattern.CASE_INSENSITIVE;
import static java.util.regex.Pattern.compile;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class EmailUtils {
	
	/**
	 * validação de email verdadeiro
	 * 
	 * @param email
	 * @return
	 */
	public static boolean isValidEmailAddressRegex(String email) {
		if (isNotEmpty(email)) {
			return compile(EMAIL_PATTERN, CASE_INSENSITIVE).matcher(email).matches();
		}
		return false;
	}
}
