package br.com.ghnetsoft.principal.utilitario.validacao;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.BARRA;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.PONTO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.REGEX_VAZIO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.TRACO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.VAZIO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ZERO;
import static br.com.ghnetsoft.principal.utilitario.stringUtils.StringUtilsGH.preencheStringComCaracter;
import static br.com.ghnetsoft.principal.utilitario.stringUtils.StringUtilsGH.removeAcento;
import static br.com.ghnetsoft.principal.utilitario.stringUtils.StringUtilsGH.trocarString;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class CpfCnpjUtils {

	private static final Integer[] PESO_CPF = { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
	private static final Integer[] PESO_CNPJ = { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };

	/**
	 * validacao de cnpj ou cpf
	 * 
	 * @param cpfCnpj
	 * @return
	 */
	public static boolean isValidCnpjCpf(String cpfCnpj) {
		if (isEmpty(cpfCnpj)) {
			return false;
		} else {
			cpfCnpj = trocaCnpjCpf(cpfCnpj);
			if (cpfCnpj.length() == 11) {
				return isValidCPF(cpfCnpj);
			} else if (cpfCnpj.length() == 14) {
				return isValidCNPJ(cpfCnpj);
			} else {
				return false;
			}
		}
	}

	/**
	 * Retira os caracteres ".", "/" e "-" do documento
	 * 
	 * @param cpfCnpj
	 * @return
	 */
	public static String trocaCnpjCpf(String cpfCnpj) {
		cpfCnpj = trocarString(cpfCnpj, REGEX_VAZIO + PONTO, VAZIO);
		cpfCnpj = trocarString(cpfCnpj, TRACO, VAZIO);
		return trocarString(cpfCnpj, BARRA, VAZIO);
	}

	/**
	 * retorna numero com a mascara do CNPJ ou CPF
	 * 
	 * @param valor
	 * @return
	 */
	public static String mascararToCnpjCpf(String valor) {
		if (isNotEmpty(valor)) {
			Integer tamanho = removeAcento(valor).length();
			if (tamanho == 11) {
				return mascararToCpf(valor);
			} else if (tamanho == 14) {
				return mascararToCnpj(valor);
			}
		}
		return null;
	}

	/**
	 * calculo de digito
	 * 
	 * @param str
	 * @param peso
	 * @return
	 */
	private static Integer calcularDigito(String str, Integer[] peso) {
		Integer soma = 0;
		for (Integer indice = str.length() - 1, digito; indice >= 0; indice--) {
			digito = Integer.parseInt(str.substring(indice, indice + 1));
			soma += digito * peso[peso.length - str.length() + indice];
		}
		soma = 11 - soma % 11;
		return soma > 9 ? 0 : soma;
	}

	/**
	 * validacao de cpf
	 * 
	 * @param cpf
	 * @return
	 */
	private static boolean isValidCPF(String cpf) {
		Integer digito1 = calcularDigito(cpf.substring(0, 9), PESO_CPF);
		Integer digito2 = calcularDigito(cpf.substring(0, 9) + digito1, PESO_CPF);
		return cpf.equals(cpf.substring(0, 9) + digito1.toString() + digito2.toString());
	}

	/**
	 * validacao de cnpj
	 * 
	 * @param cnpj
	 * @return
	 */
	private static boolean isValidCNPJ(String cnpj) {
		Integer digito1 = calcularDigito(cnpj.substring(0, 12), PESO_CNPJ);
		Integer digito2 = calcularDigito(cnpj.substring(0, 12) + digito1, PESO_CNPJ);
		return cnpj.equals(cnpj.substring(0, 12) + digito1.toString() + digito2.toString());
	}

	/**
	 * retorna numero com a mascara do cnpj
	 * 
	 * @param valor
	 * @return
	 */
	private static String mascararToCnpj(String valor) {
		valor = preencheStringComCaracter(valor.toString(), ZERO, 14);
		return valor.substring(0, 2) + PONTO + valor.substring(2, 5) + PONTO + valor.substring(5, 8) + BARRA
				+ valor.substring(8, 12) + TRACO + valor.substring(12, 14);
	}

	/**
	 * retorna numero com a mascara do cpf
	 * 
	 * @param valor
	 * @return
	 */
	private static String mascararToCpf(String valor) {
		valor = preencheStringComCaracter(valor.toString(), ZERO, 11);
		return valor.substring(0, 3) + PONTO + valor.substring(3, 6) + PONTO + valor.substring(6, 9) + TRACO
				+ valor.substring(9, 11);
	}
}
