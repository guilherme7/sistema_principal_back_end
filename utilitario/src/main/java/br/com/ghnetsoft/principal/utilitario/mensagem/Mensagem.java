package br.com.ghnetsoft.principal.utilitario.mensagem;

import static java.text.MessageFormat.format;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

/**
 * @author Guilherme C Lopes
 */
@Getter
@ToString(of = { "type", "chave", "texto" })
@EqualsAndHashCode(of = { "chave" })
public class Mensagem implements Comparable<Mensagem>, Serializable {

	private static final long serialVersionUID = 4077401356823475188L;

	private final String texto;
	private final String chave;
	private final TipoMensagemEnum type;

	/**
	 * construtor
	 * 
	 * @param type
	 * @param chave
	 * @param texto
	 */
	public Mensagem(TipoMensagemEnum type, String chave, String texto) {
		this.type = type;
		this.chave = chave;
		this.texto = texto;
	}

	/**
	 * construtor
	 * 
	 * @param type
	 * @param chave
	 * @param pattern
	 * @param arguments
	 */
	public Mensagem(TipoMensagemEnum type, String chave, String pattern, Object... arguments) {
		this(type, chave, format(pattern, arguments));
	}

	/**
	 * comparação de objeto
	 */
	@Override
	public int compareTo(Mensagem that) {
		if (that == null) {
			return 1;
		}
		return chave.compareTo(that.chave);
	}
}
