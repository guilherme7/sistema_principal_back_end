package br.com.ghnetsoft.principal.utilitario.arquivo;

import static br.com.ghnetsoft.principal.utilitario.arquivo.ConversorFileUtil.converterFileEmArrayBytes;
import static lombok.AccessLevel.PRIVATE;
import static org.apache.commons.lang3.builder.ToStringStyle.SIMPLE_STYLE;

import java.io.File;
import java.io.IOException;

import org.apache.commons.lang3.builder.ToStringBuilder;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
@Builder()
@AllArgsConstructor(access = PRIVATE)
public class ArquivoDownload {

	private byte[] conteudo;
	private String extensao;
	private String mimetype;
	private String nome;

	/**
	 * To string
	 */
	@Override
	public String toString() {
		return new ToStringBuilder(this, SIMPLE_STYLE).append(this.nome).append(this.mimetype)
				.append(this.conteudo.length).toString();
	}

	/**
	 * Gera o arquivo para fazer Download
	 * 
	 * @param file
	 * @param nomeArquivo
	 * @param mimetype
	 * @param extensao
	 * @return
	 * @throws IOException
	 */
	public static ArquivoDownload gerar(File file, String nomeArquivo, String mimetype, String extensao)
			throws IOException {
		return ArquivoDownload.builder().mimetype(mimetype).extensao(extensao).nome(nomeArquivo)
				.conteudo(converterFileEmArrayBytes(file)).build();
	}
}
