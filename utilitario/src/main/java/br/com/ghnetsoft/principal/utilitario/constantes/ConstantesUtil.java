package br.com.ghnetsoft.principal.utilitario.constantes;

import java.util.Locale;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class ConstantesUtil {

	public static final String DEVE_ESCOLHER = "Deve escolher ";

	public static final String PARENTESES_DIREITO = "(";
	public static final String PARENTESES_ESQUERDO = ")";
	public static final String PONTO = ".";
	public static final String DOIS_PONTOS = ":";
	public static final String REGEX_PONTO = "\\.";
	public static final String REGEX_VAZIO = "\\";
	public static final String REMOVE_ACENTO = "[^\\p{ASCII}]";
	public static final String BARRA = "/";
	public static final String TRACO = "-";
	public static final String VAZIO = "";
	public static final String ZERO = "0";
	public static final String ESPACO = " ";
	public static final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
	public static final String CARACTERES_A_Z = "ABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890";
	public static final String CARACTERES_ESPECIAIS = "!@#$%&*()_-=+{[]};:/?<>,.|";
	public static final String CARACTERES_DIGITOS = "[^0-9]";

	public static final String UTF_8 = "UTF-8";
	public static final String MD5 = "MD5";
	public static final String FILTRO_EXCLUSAO_LOGICA = "ST_REGISTRO IS NULL OR ST_REGISTRO <> 'EXCLUIDO'";
	public static final Locale LOCAL_BRAZIL = new Locale("pt", "BR");
	public static final String PADRAO_REAL = "###,###,##0.00";

	public static final int ZERO_CASA_DECIMAL = 0;
	public static final int UMA_CASA_DECIMAL = 1;
	public static final int DUAS_CASAS_DECIMAIS = 2;
	public static final int TRES_CASAS_DECIMAIS = 3;

	public static final int MAIORIDADE = 18;
	public static final int REGISTROS_POR_PAGINA_PADRAO = 100;
	/**
	 * Relatorios
	 */
	public static final String INLINE_FILENAME = "inline; filename=";
	public static final String CONTENT_DISPOSITION = "Content-disposition";
	public static final String RELATORIOS = "/relatorios/";
	public static final String JASPER = ".jasper";
	public static final String APPLICATION_PDF = "application/x-pdf";
	public static final String NOME_RELATORIO = "nomeRelatorio";
	public static final String IMPRESSAO_POR_ANTES = "Impressão por: ";
	public static final String IMPRESSAO_POR = "impressaoPor";
	public static final String EMPRESA = "empresa";
	/**
	 * Outros
	 */
	public static final String APLICACAO_DO_AUDITORIA_PARA_A_CLASSE = "Aplicação do auditoria para a classe: ";
	/**
	 * Mensagens comuns
	 */
	public static final String NAO_EXISTEM_RESULTADOS_COM_ESTES_PARAMETROS_DE_PESQUISA = "Não existem resultados com estes parâmetros de pesquisa !";
	public static final String PESQUISA_REALIZADA_COM_SUCESSO = "Pesquisa realizada com sucesso !";
	public static final String PARTE_MENSAGEM_EXCLUSAO = " não existente em nossa base de dados !";
	public static final String PARTE_MENSAGEM_DADO_EXISTENTE_BASE_DADOS = " existente em nossa base de dados !";
	public static final String REGISTRO_INCLUIDO_COM_SUCESSO = "Registro incluído com sucesso !";
	public static final String REGISTRO_ALTERADO_COM_SUCESSO = "Registro alterado com sucesso !";
	public static final String USUARIO_SO_PODE_ESTAR_INCLUINDO = "usuario-so-pode-estar-incluindo";
	public static final String USUARIO_SO_PODE_ESTAR_ALTERANDO = "usuario-so-pode-estar-alterando";
	public static final String USUARIO_SO_PODE_ESTAR_EXCLUINDO = "usuario-so-pode-estar-excluindo";
	public static final String NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_INCLUINDO = "Neste momento usuário só pode estar incluindo !";
	public static final String NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_ALTERANDO = "Neste momento usuário só pode estar alterando !";
	public static final String NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_EXCLUINDO = "Neste momento usuário só pode estar excluindo !";
	public static final String E_OBRIGATORIA = "é obrigatória !";
	public static final String E_OBRIGATORIO = "é obrigatório !";
	public static final String INVALIDO_OU_EM_BRANCO = "inválido ou em branco !";
	public static final String MENSAGEM_SEM_FILTRO_PESQUISA = DEVE_ESCOLHER + "no mínimo um parâmetro para pesquisar !";
	public static final String MENSAGEM_NAO_OBRIGATORIA = "não é obrigatório. Mas quando preenchido não pode ser maior que ";
	public static final String CARACTERES = "caracteres !";
	public static final String REQUISICAO_SO_PARAR_ALTERAR = "Esta requisição só pode ser usada para alterar !";
	public static final String REQUISICAO_SO_PARAR_INCLUIR = "Esta requisição só pode ser usada para incluir !";
	public static final String SALVAR_COM_SUCESSO = "Registro salvo com sucesso !";
	public static final String SALVAR_COM_ERRO = "Existem erros ao tentar salvar este registro: ";
	public static final String REGISTRO_EXCLUIDO_COM_SUCESSO = "Registro excluído com sucesso !";
	public static final String JA_ESTA_EXCLUIDO = "já está excluído !";
	public static final String ERRO_EM = "Erro em ";
	public static final String NAO_PODE_SER_MAIOR_QUE = " não pode ser maior que ";
	public static final String DEVE_ESTAR_PREENCHIDO = " deve estar preenchido !";
	public static final String POR_ID = " por id ";
	public static final String PESQUISAR = " pesquisar ";
	public static final String EXCLUIR = " excluir ";
	public static final String ALTERAR = " alterar ";
	public static final String INCLUIR = " incluir ";
	public static final String BUSCAR = " buscar ";
	public static final String UMA = " uma ";

	// chaves
	public static final String ERRO = "erro";
	public static final String POR_ID_CHAVE = "-por-id";
	public static final String PESQUISAR_CHAVE = "-pesquisar";
	public static final String EXCLUIR_CHAVE = "-excluir";
	public static final String ALTERAR_CHAVE = "-alterar";
	public static final String EXCLUIR_ALTERAR_PESQUISAR_CHAVE = "-excluir-alterar-pesquisar";
	public static final String JA_ESTA_EXCLUIDO_CHAVE = "-ja-excluido";
	public static final String NAO_PREENCHIDA_CHAVE = "-nao-preenchida";
	public static final String TAMANHO_ERRADO_CHAVE = "-tamanho-errado";
	public static final String EXCLUIDO_SUCESSO = "excluido-com-sucesso";
	public static final String INCLUIR_CHAVE = "-incluir";
	public static final String EXISTENTE_BASE_DADOS = "-existente-base-dados-";

	/**
	 * Sistemas em comum
	 */
	public static final String ROLE = "ROLE_";
	public static final String UNAUTHORIZED = "Unauthorized";
	public static final String AUTHORIZATION = "Authorization";
	public static final String CLAIN_KEIN_USERNAME = "sub";
	public static final String CLAIN_KEY_CREATED = "created";
	public static final String CLAIN_KEY_EXPIRED = "exp";
	public static final String BUSCA_VAZIA = "busca-vazia";
	public static final String BUSCA_COM_REGISTROS = "busca-com-registros";
	public static final String SALVO_COM_SUCESSO = "salvo-com-sucesso";
	public static final String EXCLUIDO_COM_SUCESSO = "excluido-com-sucesso";
	public static final String SALVO_COM_ERRO = "salvo-com-erro";
	public static final String ERRO_GERAL = "-erro-geral";

	// Grupos de usuários quando inicia o sistema em desenvolvimento
	public static final String ADMINISTRADOR = "Administrador";
	public static final String COMUM = "Comun";

	public static final String PORCENTAGEM = "%";
	public static final String EXCLAMACAO = "!";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
}
