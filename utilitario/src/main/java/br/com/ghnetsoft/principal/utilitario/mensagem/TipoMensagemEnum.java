package br.com.ghnetsoft.principal.utilitario.mensagem;

/**
 * @author Guilherme C Lopes
 */
public enum TipoMensagemEnum {

	CUSTOM, ERROR, INFORMATION, SUCCESS, WARNING;
}
