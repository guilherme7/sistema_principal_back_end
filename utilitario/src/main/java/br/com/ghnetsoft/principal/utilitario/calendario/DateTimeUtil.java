package br.com.ghnetsoft.principal.utilitario.calendario;

import static org.joda.time.format.DateTimeFormat.forPattern;

import java.util.Date;

import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
public class DateTimeUtil {

	public static final String DD_MM_YYYY_HH_MM_SS_MS = "dd/MM/yyyy HH:mm:ss.SSS";
	public static final String DD_MM_YYYY_HH_MM_SS = "dd/MM/yyyy HH:mm:ss";
	public static final String DD_MM_YYYY = "dd/MM/yyyy";
	public static final String YYYY_MM_DD_HH_MM_SS_MS = "yyyyMMddHHmmssSSS";
	public static final String HH_MM_SS = "HH:mm:SS";

	/**
	 * retorna a conversão para String de uma data no formato pelo segundo parametro
	 * 
	 * @param localDateTime
	 * @param pattern
	 * @return
	 */
	public static String converterLocalDateTimeParaString(LocalDateTime localDateTime, String pattern) {
		return localDateTime.toString(pattern);
	}

	/**
	 * retorna a hora em uma String
	 * 
	 * @param time
	 * @return
	 */
	public static String converterLocalTimeParaString(LocalTime time) {
		return time.toString(HH_MM_SS);
	}

	/**
	 * retorna a conversão para LocalDateTime de uma data no formato pelo segundo
	 * parametro
	 * 
	 * @param localDateTime
	 * @param pattern
	 * @return
	 */
	public static LocalDateTime converterStringParaLocalDateTime(String localDateTime, String pattern) {
		return LocalDateTime.parse(localDateTime, forPattern(pattern));
	}

	/**
	 * Retorna a data no formato Date
	 * 
	 * @param data
	 * @return
	 */
	public static Date converterJavaDateHora(LocalDateTime data) {
		return data.toDate();
	}

	/**
	 * Retorna se a data/ hora e inferior a data / hora atual
	 * 
	 * @param dataChecada
	 * @return
	 */
	public static Boolean dataHoraEstaMenorQueAtual(LocalDateTime dataChecada) {
		return dataChecada != null ? !dataChecada.isBefore(LocalDateTime.now()) : true;
	}

	/**
	 * retorna a conversão para LocalDateTime de uma Date
	 * 
	 * @param data
	 * @return
	 */
	public static LocalDateTime converterDateParaLocalDateTime(Date data) {
		return LocalDateTime.fromDateFields(data);
	}
}
