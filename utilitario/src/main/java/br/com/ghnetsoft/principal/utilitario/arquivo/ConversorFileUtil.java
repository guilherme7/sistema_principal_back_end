package br.com.ghnetsoft.principal.utilitario.arquivo;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import lombok.experimental.UtilityClass;

/**
 * @author Guilherme C Lopes
 */
@UtilityClass
class ConversorFileUtil {

	/**
	 * Converte arquivo File em byte
	 * 
	 * @param file
	 * @return
	 * @throws IOException
	 */
	static byte[] converterFileEmArrayBytes(File file) throws IOException {
		byte[] bytesArray = new byte[(int) file.length()];
		FileInputStream fis = new FileInputStream(file);
		fis.read(bytesArray);
		fis.close();
		return bytesArray;
	}
}
