package br.com.ghnetsoft.principal.utilitario.mensagem;

import java.util.TreeSet;

/**
 * @author Guilherme C Lopes
 */
public class Mensagens extends TreeSet<Mensagem> {

	private static final long serialVersionUID = 4609610094846560652L;

	/**
	 * incser a lista de mensagens
	 * 
	 * @param type
	 * @return
	 */
	public Mensagens extractByType(TipoMensagemEnum type) {
		Mensagens mensagens = new Mensagens();
		for (Mensagem mensagem : this) {
			if (mensagem.getType().equals(type)) {
				mensagens.add(mensagem);
			}
		}
		return mensagens;
	}
}
