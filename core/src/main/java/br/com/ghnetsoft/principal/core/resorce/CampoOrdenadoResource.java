package br.com.ghnetsoft.principal.core.resorce;

import static lombok.AccessLevel.PUBLIC;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Builder
@Getter
@Setter
@NoArgsConstructor(access = PUBLIC)
@AllArgsConstructor(access = PUBLIC)
public class CampoOrdenadoResource {

	private String direcao;
	private String campo;
}
