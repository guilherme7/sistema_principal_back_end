package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum TipoRelatorioEnum {

	PDF("PDF"), HTML("HTML"), EXCEL("EXCEL"), WORD("WORD");

	private String descricao;

	TipoRelatorioEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o TipoRelatorioEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static TipoRelatorioEnum buscaTipoRelatorioEnum(String descricao) {
		for (TipoRelatorioEnum enun : TipoRelatorioEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
