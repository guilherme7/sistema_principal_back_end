package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum SimNaoEnum {

	NAO("Não", "#FF6347"), SIM("Sim", "#6495ED");

	private String descricao;
	private String cor;

	SimNaoEnum(String descricao, String cor) {
		this.descricao = descricao;
		this.cor = cor;
	}

	/**
	 * busca o SimNaoEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static SimNaoEnum buscaSimNaoEnum(String descricao) {
		for (SimNaoEnum enun : SimNaoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
