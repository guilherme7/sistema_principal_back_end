package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum DiaSemanaEnum {

	DOMINGO("Domingo"), SEGUNDA("Segunda feira"), TERCA("Terça feira"), QUARTA("Quarta feira"), QUINTA("Quinta feira"),
	SEXTA("Sexta feira"), SABADO("Sábado");

	private String descricao;

	DiaSemanaEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o DiaSemanaEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static DiaSemanaEnum buscaDiaSemanaEnum(String descricao) {
		for (DiaSemanaEnum enun : DiaSemanaEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
