package br.com.ghnetsoft.principal.core.regras.impl;

import static br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum.EXCLUIDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.JA_ESTA_EXCLUIDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.JA_ESTA_EXCLUIDO_CHAVE;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_ALTERANDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_EXCLUINDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_INCLUINDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.USUARIO_SO_PODE_ESTAR_ALTERANDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.USUARIO_SO_PODE_ESTAR_EXCLUINDO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.USUARIO_SO_PODE_ESTAR_INCLUINDO;
import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.ERROR;

import java.util.Collection;

import org.springframework.stereotype.Component;

import br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum;
import br.com.ghnetsoft.principal.core.modelo.ProPrincipal;
import br.com.ghnetsoft.principal.core.regras.ValidarIncluirAlterarExcluir;
import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;

/**
 * @author Guilherme C Lopes
 */
@Component
public class ValidarIncluirAlterarExcluirImpl implements ValidarIncluirAlterarExcluir {

	/**
	 * validação de resouce ao incluir
	 * 
	 * @param id
	 * @param mensagens
	 */
	@Override
	public void validarIncluir(Long id, Collection<Mensagem> mensagens) {
		if (id != null) {
			mensagens.add(new Mensagem(ERROR, USUARIO_SO_PODE_ESTAR_INCLUINDO,
					NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_INCLUINDO));
		}
	}

	/**
	 * validação de resouce ao alterar
	 * 
	 * @param id
	 * @param mensagens
	 */
	@Override
	public void validarAlterar(Long id, Collection<Mensagem> mensagens) {
		if (id == null) {
			mensagens.add(new Mensagem(ERROR, USUARIO_SO_PODE_ESTAR_ALTERANDO,
					NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_ALTERANDO));
		}
	}

	/**
	 * validação de resouce ao alterar com ID diferente
	 * 
	 * @param objeto
	 * @param mensagens
	 */
	@Override
	public void validarAlterarRegistroIdnulo(ProPrincipal objeto, Collection<Mensagem> mensagens) {
		if (objeto == null) {
			mensagens.add(new Mensagem(ERROR, "objeto-invalido-nulo",
					"Este registro não pode ser alterado, favor consultar a administração !"));
		}
	}

	/**
	 * validação de resouce ao excluir
	 * 
	 * @param id
	 * @param mensagens
	 */
	@Override
	public void validarExcluir(Long id, Collection<Mensagem> mensagens) {
		if (id == null) {
			mensagens.add(new Mensagem(ERROR, USUARIO_SO_PODE_ESTAR_EXCLUINDO,
					NESTE_MOMENTO_USUARIO_USUARIO_SO_PODE_ESTAR_EXCLUINDO));
		}
	}

	/**
	 * validação de resouce se ele já está excluido
	 * 
	 * @param status
	 * @param mensagens
	 * @param chave
	 * @param entidade
	 */
	@Override
	public void validarExcluirGeral(StatusDoRegistroEnum status, Collection<Mensagem> mensagens, String chave,
			String entidade) {
		if (status.equals(EXCLUIDO)) {
			mensagens.add(new Mensagem(ERROR, chave + JA_ESTA_EXCLUIDO_CHAVE, entidade + JA_ESTA_EXCLUIDO));
		}
	}
}
