package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum OperacaoTelaEnum {

	INCLUIR("Incluir"), ALTERAR("Alterar"), EXCLUIR("Excluir"), VISUALIZAR("Visualizar"), PESQUISAR("Pesquisar");

	private String descricao;

	OperacaoTelaEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o OperacaoTelaEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static OperacaoTelaEnum buscaOperacaoTelaEnum(String descricao) {
		for (OperacaoTelaEnum enun : OperacaoTelaEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
