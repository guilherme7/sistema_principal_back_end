package br.com.ghnetsoft.principal.core.modelo;

import static javax.persistence.EnumType.STRING;
import static lombok.AccessLevel.PROTECTED;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;

import br.com.ghnetsoft.principal.core.enuns.SituacaoRegistroEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@Getter
@Setter
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public abstract class Principal extends ProPrincipal implements Comparable<Principal> {

	private static final long serialVersionUID = 8505501596449541446L;

	@Enumerated(STRING)
	@Column(name = "ST_SITUACAO", nullable = false, length = 15)
	private SituacaoRegistroEnum dsSituacao;

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj instanceof Principal && this.getId() != null) {
			return this.getId().equals(((Principal) obj).getId());
		}
		return false;
	}

	@Override
	public int hashCode() {
		return super.hashCode();
	}

	public int compareTo(Principal principal) {
		if (getId() != null && principal != null && principal.getId() != null)
			return getId().compareTo(principal.getId());
		return -1;
	}
}
