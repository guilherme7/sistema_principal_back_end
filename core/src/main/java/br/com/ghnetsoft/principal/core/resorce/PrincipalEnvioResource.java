package br.com.ghnetsoft.principal.core.resorce;

import static lombok.AccessLevel.PUBLIC;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Getter
@Setter
@NoArgsConstructor(access = PUBLIC)
@AllArgsConstructor(access = PUBLIC)
public class PrincipalEnvioResource implements Serializable {

	private static final long serialVersionUID = -3115724796192662183L;

	private Long id;
	private String loginUsuario;
	private String statusDoRegistro;
}
