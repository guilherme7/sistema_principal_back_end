package br.com.ghnetsoft.principal.core.regras.impl;

import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.ERROR;
import static org.apache.commons.lang3.StringUtils.isEmpty;

import java.util.Collection;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.stereotype.Component;

import br.com.ghnetsoft.principal.core.regras.ValidarPaginacaoPesquisar;
import br.com.ghnetsoft.principal.core.resorce.PaginacaoEnvioResource;
import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;

/**
 * @author Guilherme C Lopes
 */
@Component
public class ValidarPaginacaoPesquisarImpl implements ValidarPaginacaoPesquisar {

	private static final String PAGINACAO_ERRO = "paginacao-erro";

	/**
	 * Validação de registro de paginação por pesquisa
	 * 
	 * @param resource
	 * @param mensagens
	 */
	@Override
	public void validar(PaginacaoEnvioResource resource, Collection<Mensagem> mensagens) {
		if (resource.getPaginaAtual() == null || resource.getPaginaAtual() < 0) {
			mensagens.add(new Mensagem(ERROR, PAGINACAO_ERRO, "Página atual não pode ser nulo ou menor que 0 !"));
		}
		if (resource.getQuantidadeRegistros() == null || resource.getQuantidadeRegistros() < 0) {
			mensagens.add(
					new Mensagem(ERROR, PAGINACAO_ERRO, "Quantidade de registros não pode ser nulo ou menor que 0 !"));
		}
		if (isEmpty(resource.getDirecao())) {
			mensagens.add(new Mensagem(ERROR, PAGINACAO_ERRO, "Direção não pode ser nulo ou vazio !"));
		} else {
			if (!(Direction.DESC.toString().toUpperCase().equals(resource.getDirecao().toUpperCase())
					|| Direction.ASC.toString().toUpperCase().equals(resource.getDirecao().toUpperCase()))) {
				mensagens.add(new Mensagem(ERROR, PAGINACAO_ERRO, "Direção deve ser valida !"));
			}
		}
		if (isEmpty((resource.getCampo()))) {
			mensagens.add(new Mensagem(ERROR, PAGINACAO_ERRO, "Campo não pode ser nulo ou vazio !"));
		}
	}
}
