package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum SituacaoRegistroEnum {

	EM_EDICAO("EM EDICAO"), CONCLUIDO("CONCLUIDO"), APROVADO("APROVADO");

	private String descricao;

	SituacaoRegistroEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o SituacaoRegistroEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static SituacaoRegistroEnum buscaSituacaoRegistroEnum(String descricao) {
		for (SituacaoRegistroEnum enun : SituacaoRegistroEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
