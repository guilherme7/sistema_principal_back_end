package br.com.ghnetsoft.principal.core.resorce;

import static lombok.AccessLevel.PUBLIC;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Getter
@Setter
@NoArgsConstructor(access = PUBLIC)
@AllArgsConstructor(access = PUBLIC)
public class PrincipalRetornoIdResource implements Serializable {

	private static final long serialVersionUID = -1385749046083774419L;

	private Long id;
}
