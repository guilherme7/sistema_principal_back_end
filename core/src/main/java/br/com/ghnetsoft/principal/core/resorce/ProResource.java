package br.com.ghnetsoft.principal.core.resorce;

import static br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum.buscaStatusDoRegistroEnum;

import javax.persistence.MappedSuperclass;

import br.com.ghnetsoft.principal.core.modelo.ProPrincipal;
import br.com.ghnetsoft.principal.core.modelo.auditoria.AuditMetadata;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
public abstract class ProResource {

	/**
	 * preenche os campos status do registro e id do resource
	 * 
	 * @param proPrincipal
	 * @param resource
	 */
	protected void preencherResource(ProPrincipal proPrincipal, PrincipalEnvioResource resource) {
		resource.setStatusDoRegistro(proPrincipal.getStatusDoRegistro().getDescricao());
		resource.setId(proPrincipal.getId());
	}

	/**
	 * preenche os campos statusdo registro e id da entidade
	 * 
	 * @param proPrincipal
	 * @param resource
	 */
	protected void preencherEntidade(ProPrincipal proPrincipal, PrincipalEnvioResource resource) {
		proPrincipal.setStatusDoRegistro(buscaStatusDoRegistroEnum(resource.getStatusDoRegistro()));
		proPrincipal.setId(resource.getId());
	}

	/**
	 * preenche a entidade para auditoria
	 * 
	 * @param resource
	 * @param proPrincipal
	 */
	protected void auditoria(PrincipalEnvioResource resource, ProPrincipal proPrincipal) {
		AuditMetadata metadadoAuditoria = proPrincipal.getMetadadoAuditoria();
		metadadoAuditoria.setLoginMovimentacao(resource.getLoginUsuario());
		proPrincipal.setMetadadoAuditoria(metadadoAuditoria);
	}
}
