package br.com.ghnetsoft.principal.core.resorce;

import java.util.Collection;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Getter
@Setter
public abstract class PrincipalRetornoResource extends PrincipalRetornoIdResource {

	private static final long serialVersionUID = -4187548144332184453L;

	private Collection<Mensagem> mensagens;
}
