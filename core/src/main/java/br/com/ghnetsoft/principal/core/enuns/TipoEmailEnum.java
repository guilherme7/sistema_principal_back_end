package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum TipoEmailEnum {

	PRINCIPAL("Principal"), SECUNDARIO("Secundário"), NFSE("Nota Fiscal Eletrônica");

	private String descricao;

	TipoEmailEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o TipoEmailEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static TipoEmailEnum buscaTipoEmailEnum(String descricao) {
		for (TipoEmailEnum enun : TipoEmailEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
