package br.com.ghnetsoft.principal.core.modelo;

import static javax.persistence.EnumType.STRING;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.Transient;
import javax.persistence.Version;

import org.hibernate.annotations.DynamicInsert;
import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.SelectBeforeUpdate;

import br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum;
import br.com.ghnetsoft.principal.core.modelo.auditoria.AuditableBase;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@Getter
@Setter
@DynamicUpdate
@DynamicInsert
@SelectBeforeUpdate
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public abstract class ProPrincipal extends AuditableBase {

	private static final long serialVersionUID = 3284334685219450040L;

	@Transient
	private Long id;
	@Temporal(TIMESTAMP)
	@Column(name = "TS_DATA_HORA_CADASTRO", nullable = false)
	private Date dataCadastro;
	@Version
	@Column(name = "NR_VERSAO", nullable = false)
	private Integer versao;
	@Enumerated(STRING)
	@Column(name = "ST_REGISTRO", nullable = false, length = 15)
	private StatusDoRegistroEnum statusDoRegistro;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ProPrincipal other = (ProPrincipal) obj;
		if (id == null) {
			return other.id == null;
		} else {
			return id.equals(other.id);
		}
	}
}
