package br.com.ghnetsoft.principal.core.resorce;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Getter
@Setter
public abstract class PaginacaoEnvioResource implements Serializable {

	private static final long serialVersionUID = -6012221283468899132L;

	private Integer paginaAtual;
	private Integer quantidadeRegistros;
	private String direcao;
	private String campo;
}
