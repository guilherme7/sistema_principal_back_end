package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum MesEnum {

	JANEIRO("Janeiro"), FEVEREIRO("Fevereiro"), MARCO("Março"), ABRIL("Abril"), MAIO("Maio"), JUNHO("Junho"),
	JULHO("Julho"), AGOSTO("Agosto"), SETEMBRO("Setembro"), OUTUBRO("Outubro"), NOVEMBRO("Novembro"),
	DEZEMBRO("Dezembro");

	private String descricao;

	MesEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o MesEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static MesEnum buscaMesEnum(String descricao) {
		for (MesEnum enun : MesEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}

	/**
	 * busca o MesEnum pela valor inteiro
	 * 
	 * @param descricao
	 * @return
	 */
	public static MesEnum buscaMesEnum(Integer indice) {
		for (MesEnum enun : MesEnum.values()) {
			if (enun.ordinal() == indice) {
				return enun;
			}
		}
		return null;
	}
}
