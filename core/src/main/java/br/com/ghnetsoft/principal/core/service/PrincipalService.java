package br.com.ghnetsoft.principal.core.service;

import static br.com.ghnetsoft.principal.core.enuns.SituacaoRegistroEnum.EM_EDICAO;
import static br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum.ATIVO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.APLICACAO_DO_AUDITORIA_PARA_A_CLASSE;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.DOIS_PONTOS;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ESPACO;
import static lombok.AccessLevel.PROTECTED;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.MappedSuperclass;

import br.com.ghnetsoft.principal.core.modelo.Principal;
import br.com.ghnetsoft.principal.core.modelo.auditoria.AuditMetadata;
import lombok.AllArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@CommonsLog
@AllArgsConstructor(access = PROTECTED)
public abstract class PrincipalService implements Serializable {

	private static final long serialVersionUID = -9135282744336017857L;

	/**
	 * Aplica a auditoria na entidade que vai ser salva no bvanco de dados
	 * 
	 * @param objeto
	 */
	protected void aplicacaoAuditoria(Object objeto) {
		if (objeto instanceof Principal) {
			Principal ghEntity = (Principal) objeto;
			AuditMetadata metadadoAuditoria = preencheAuditoria(ghEntity);
			if (ghEntity.getStatusDoRegistro() == null) {
				ghEntity.setStatusDoRegistro(ATIVO);
				ghEntity.setDsSituacao(EM_EDICAO);
				ghEntity.setDataCadastro(new Date());
			}
			if (ghEntity.getVersao() == null) {
				ghEntity.setVersao(0);
			}
			ghEntity.setMetadadoAuditoria(metadadoAuditoria);
		}
		log.info(APLICACAO_DO_AUDITORIA_PARA_A_CLASSE + ESPACO + getClass().getName() + ESPACO + DOIS_PONTOS
				+ objeto.getClass().getName());
	}

	/**
	 * preenche a auditoria da entidade que vai ser salva no banco de dados
	 * 
	 * @param ghEntity
	 * @return
	 */
	private AuditMetadata preencheAuditoria(Principal ghEntity) {
		AuditMetadata metadadoAuditoria = AuditMetadata.builder().build();
		metadadoAuditoria.setIpMovimentacao("localhost");
		metadadoAuditoria.setLoginMovimentacao("02679500636");// ghEntity.getMetadadoAuditoria().getLoginMovimentacao());
		return metadadoAuditoria;
	}
}
