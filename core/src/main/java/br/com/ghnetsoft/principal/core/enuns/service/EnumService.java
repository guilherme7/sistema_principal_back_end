package br.com.ghnetsoft.principal.core.enuns.service;

import java.util.Collection;

import br.com.ghnetsoft.principal.core.resorce.EnumResource;

/**
 * @author Guilherme C Lopes
 */
public interface EnumService {

	/**
	 * Busca os status do registro
	 * 
	 * @return
	 */
	Collection<EnumResource> statusRegistros();

	/**
	 * Busca os pessoas tipos
	 * 
	 * @return
	 */
	Collection<EnumResource> pessoasTipos();

	/**
	 * Busca os tipos de emails
	 * 
	 * @return
	 */
	Collection<EnumResource> tiposEmails();

	/**
	 * Busca os tipos de telefone
	 */
	Collection<EnumResource> tiposTelefones();

	/**
	 * busca as operações de bancos
	 * 
	 * @return
	 */
	Collection<EnumResource> operacacoesBancos();

	/**
	 * Busca os sim não
	 * 
	 * @return
	 */
	Collection<EnumResource> simNao();

	/**
	 * Busca as situações do registros
	 * 
	 * @return
	 */
	Collection<EnumResource> situacoesRegistros();

	/**
	 * Busca os tipos de relatórios
	 * 
	 * @return
	 */
	Collection<EnumResource> tiposRelatorios();

	/**
	 * Busca os tipos de pessoas
	 * 
	 * @return
	 */
	Collection<EnumResource> tiposPessoas();

	/**
	 * Busca os dias de semana
	 * 
	 * @return
	 */
	Collection<EnumResource> diasSemanas();

	/**
	 * Busca as operações em telas
	 * 
	 * @return
	 */
	Collection<EnumResource> operacoesTelas();

	/**
	 * Busca os meses
	 * 
	 * @return
	 */
	Collection<EnumResource> meses();
}
