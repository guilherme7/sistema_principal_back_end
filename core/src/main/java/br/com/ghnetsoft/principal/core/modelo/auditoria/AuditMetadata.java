package br.com.ghnetsoft.principal.core.modelo.auditoria;

import static javax.persistence.AccessType.FIELD;
import static javax.persistence.EnumType.STRING;
import static javax.persistence.TemporalType.TIMESTAMP;
import static lombok.AccessLevel.PROTECTED;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Access;
import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.Enumerated;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

import org.hibernate.envers.Audited;

import br.com.ghnetsoft.principal.core.enuns.OperacacoBancoLogEnum;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@Getter
@Setter
@Embeddable
@Access(FIELD)
@Audited
@Builder
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public class AuditMetadata implements Serializable {

	private static final long serialVersionUID = -7494237554968808847L;

	@Temporal(TIMESTAMP)
	@Column(name = "TS_MOVIMENTACAO", nullable = false)
	private Date dataMovimentacao;
	@Column(name = "CD_LOGIN_MOVIMENTACAO", length = 300, nullable = false)
	protected String loginMovimentacao;
	@Column(name = "IP_MOVIMENTACAO", length = 40, nullable = false)
	protected String ipMovimentacao;
	@Column(name = "TP_OPERACAO", length = 10, nullable = false)
	@Enumerated(STRING)
	protected OperacacoBancoLogEnum tipoOperacao;
}
