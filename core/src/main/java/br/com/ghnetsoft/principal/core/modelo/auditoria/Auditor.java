package br.com.ghnetsoft.principal.core.modelo.auditoria;

import static br.com.ghnetsoft.principal.core.enuns.OperacacoBancoLogEnum.ALTERACAO;
import static br.com.ghnetsoft.principal.core.enuns.OperacacoBancoLogEnum.INCLUSAO;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.APLICACAO_DO_AUDITORIA_PARA_A_CLASSE;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.DOIS_PONTOS;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.ESPACO;
import static lombok.AccessLevel.PROTECTED;

import java.util.Date;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.extern.apachecommons.CommonsLog;

/**
 * @author Guilherme C Lopes
 */
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
@CommonsLog
class Auditor {

	private AuditMetadata auditData = new AuditMetadata();

	/**
	 * Audita a inclusão
	 * 
	 * @param entidade
	 */
	@PrePersist
	public void auditaInclusao(AuditableBase entidade) {
		auditData.setTipoOperacao(INCLUSAO);
		audita(entidade);
	}

	/**
	 * Audita a alteração
	 * 
	 * @param entidade
	 */
	@PreUpdate
	public void auditaAlteracao(AuditableBase entidade) {
		auditData.setTipoOperacao(ALTERACAO);
		audita(entidade);
	}

	/**
	 * Preenche a aufitoria da entidade para salvar
	 * 
	 * @param entidade
	 */
	private void audita(AuditableBase entidade) {
		auditData.setDataMovimentacao(new Date());
		auditData.setIpMovimentacao(entidade.getMetadadoAuditoria().getIpMovimentacao());
		auditData.setLoginMovimentacao(entidade.getMetadadoAuditoria().getLoginMovimentacao());
		log.info(APLICACAO_DO_AUDITORIA_PARA_A_CLASSE + ESPACO + getClass().getName() + ESPACO + DOIS_PONTOS
				+ entidade.getClass().getName());
		entidade.setMetadadoAuditoria(auditData);
	}
}
