package br.com.ghnetsoft.principal.core.resorce;

import static lombok.AccessLevel.PRIVATE;
import static lombok.AccessLevel.PUBLIC;

import java.io.Serializable;
import java.util.Collection;
import java.util.Map;

import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@Builder
@Getter
@Setter
@NoArgsConstructor(access = PRIVATE)
@AllArgsConstructor(access = PUBLIC)
public class PaginacaoRetornoResource implements Serializable {

	private static final long serialVersionUID = 6390242688979996790L;

	private Integer tamanhoRegistros;
	private boolean existeRetorno;
	private boolean existeProximo;
	private boolean existeAnterior;
	private boolean existeUltimo;
	private boolean existePrimeiro;
	private Collection<Mensagem> mensagens;
	private Integer proximo;
	private Integer numeroPagina;
	private Integer paginaAnterior;
	private Integer paginaTamanho;
	private Map<String, Object> parametros;
	private CampoOrdenadoResource campoOrdenado;
	private Long totalElementos;
	private Integer totalRegistros;
}
