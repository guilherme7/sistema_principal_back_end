package br.com.ghnetsoft.principal.core.regras;

import java.util.Collection;

import br.com.ghnetsoft.principal.core.resorce.PaginacaoEnvioResource;
import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;

/**
 * @author Guilherme C Lopes
 */
public interface ValidarPaginacaoPesquisar {

	/**
	 * Validação de registro de paginação por pesquisa
	 * 
	 * @param resource
	 * @param mensagens
	 */
	void validar(PaginacaoEnvioResource resource, Collection<Mensagem> mensagens);
}
