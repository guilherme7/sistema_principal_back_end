package br.com.ghnetsoft.principal.core.enuns.service.impl;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.stereotype.Service;

import br.com.ghnetsoft.principal.core.enuns.DiaSemanaEnum;
import br.com.ghnetsoft.principal.core.enuns.MesEnum;
import br.com.ghnetsoft.principal.core.enuns.OperacacoBancoLogEnum;
import br.com.ghnetsoft.principal.core.enuns.OperacaoTelaEnum;
import br.com.ghnetsoft.principal.core.enuns.PessoaTipoEnum;
import br.com.ghnetsoft.principal.core.enuns.PessoaTipoPessoaEnum;
import br.com.ghnetsoft.principal.core.enuns.SimNaoEnum;
import br.com.ghnetsoft.principal.core.enuns.SituacaoRegistroEnum;
import br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum;
import br.com.ghnetsoft.principal.core.enuns.TipoEmailEnum;
import br.com.ghnetsoft.principal.core.enuns.TipoRelatorioEnum;
import br.com.ghnetsoft.principal.core.enuns.TipoTelefoneEnum;
import br.com.ghnetsoft.principal.core.enuns.service.EnumService;
import br.com.ghnetsoft.principal.core.resorce.EnumResource;

/**
 * @author Guilherme C Lopes
 */
@Service
public class EnumServiceImpl implements EnumService {

	/**
	 * Busca os status do registro
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> statusRegistros() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (StatusDoRegistroEnum enun : StatusDoRegistroEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os pessoas tipos
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> pessoasTipos() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (PessoaTipoEnum enun : PessoaTipoEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os tipos de emails
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> tiposEmails() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (TipoEmailEnum enun : TipoEmailEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os tipos de telefone
	 */
	@Override
	public Collection<EnumResource> tiposTelefones() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (TipoTelefoneEnum enun : TipoTelefoneEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * busca as operações de bancos
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> operacacoesBancos() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (OperacacoBancoLogEnum enun : OperacacoBancoLogEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os sim não
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> simNao() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (SimNaoEnum enun : SimNaoEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca as situações do registros
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> situacoesRegistros() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (SituacaoRegistroEnum enun : SituacaoRegistroEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os tipos de relatórios
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> tiposRelatorios() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (TipoRelatorioEnum enun : TipoRelatorioEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os tipos de pessoas
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> tiposPessoas() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (PessoaTipoPessoaEnum enun : PessoaTipoPessoaEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os dias de semana
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> diasSemanas() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (DiaSemanaEnum enun : DiaSemanaEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca as operações em telas
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> operacoesTelas() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (OperacaoTelaEnum enun : OperacaoTelaEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}

	/**
	 * Busca os meses
	 * 
	 * @return
	 */
	@Override
	public Collection<EnumResource> meses() {
		Collection<EnumResource> retorno = new ArrayList<>();
		for (MesEnum enun : MesEnum.values()) {
			retorno.add(EnumResource.builder().key(enun.name()).texto(enun.getDescricao()).build());
		}
		return retorno;
	}
}
