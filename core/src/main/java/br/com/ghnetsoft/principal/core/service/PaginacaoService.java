package br.com.ghnetsoft.principal.core.service;

import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.BUSCA_COM_REGISTROS;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.BUSCA_VAZIA;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.NAO_EXISTEM_RESULTADOS_COM_ESTES_PARAMETROS_DE_PESQUISA;
import static br.com.ghnetsoft.principal.utilitario.constantes.ConstantesUtil.PESQUISA_REALIZADA_COM_SUCESSO;
import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.ERROR;
import static br.com.ghnetsoft.principal.utilitario.mensagem.TipoMensagemEnum.SUCCESS;
import static lombok.AccessLevel.PROTECTED;
import static org.springframework.data.domain.PageRequest.of;
import static org.springframework.data.domain.Sort.by;
import static org.springframework.data.domain.Sort.Direction.DESC;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

import javax.persistence.MappedSuperclass;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;

import br.com.ghnetsoft.principal.core.resorce.CampoOrdenadoResource;
import br.com.ghnetsoft.principal.core.resorce.PaginacaoEnvioResource;
import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;
import lombok.AllArgsConstructor;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@AllArgsConstructor(access = PROTECTED)
public abstract class PaginacaoService implements Serializable {

	private static final long serialVersionUID = 4079083917189409260L;

	/**
	 * Fazendo a ordenacao por campos da consulta
	 * 
	 * @param resource
	 * @return
	 */
	protected PageRequest paginacao(PaginacaoEnvioResource resource) {
		Sort ordenacao = by(resource.getCampo());
		if (DESC.toString().toUpperCase().equals(resource.getDirecao().toUpperCase())) {
			ordenacao = ordenacao.descending();
		} else {
			ordenacao = ordenacao.ascending();
		}
		return of(resource.getPaginaAtual(), resource.getQuantidadeRegistros(), ordenacao);
	}

	/**
	 * Preencher campos parametro ou não
	 * 
	 * @param estaPreenchido
	 * @param campoValidacao
	 * @param nomeCampo
	 * @param parametros
	 */
	public void preencherCampoParametro(Boolean estaPreenchido, Object campoValidacao, String nomeCampo,
			Map<String, Object> parametros) {
		if (estaPreenchido) {
			parametros.put(nomeCampo, campoValidacao);
		}
	}

	/**
	 * Ajusta campo ordenado
	 * 
	 * @param direcao
	 * @param campo
	 * @return
	 */
	public CampoOrdenadoResource ajustaCampoOrdenado(String direcao, String campo) {
		return new CampoOrdenadoResource(direcao.toUpperCase(), campo.toUpperCase());
	}

	/**
	 * mensagem padrão de retorno de pesquisa
	 * 
	 * @param existeRegistros
	 * @return
	 */
	public Collection<Mensagem> mensagemRetornoPesquisa(Boolean existeRegistros) {
		Collection<Mensagem> mensagens = new ArrayList<>();
		if (!existeRegistros) {
			mensagens.add(new Mensagem(ERROR, BUSCA_VAZIA, NAO_EXISTEM_RESULTADOS_COM_ESTES_PARAMETROS_DE_PESQUISA));
		} else {
			mensagens.add(new Mensagem(SUCCESS, BUSCA_COM_REGISTROS, PESQUISA_REALIZADA_COM_SUCESSO));
		}
		return mensagens;
	}
}
