package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum TipoTelefoneEnum {

	FIXO("Fixo"), CELULAR("Celular"), FAX("Fax"), PABX("Pabx");

	private String descricao;

	TipoTelefoneEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o TipoTelefoneEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static TipoTelefoneEnum buscaTipoTelefoneEnum(String descricao) {
		for (TipoTelefoneEnum enun : TipoTelefoneEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
