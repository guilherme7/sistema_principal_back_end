package br.com.ghnetsoft.principal.core.modelo.auditoria;

import static lombok.AccessLevel.PROTECTED;

import java.io.Serializable;

import javax.persistence.Embedded;
import javax.persistence.EntityListeners;
import javax.persistence.MappedSuperclass;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author Guilherme C Lopes
 */
@MappedSuperclass
@EntityListeners(Auditor.class)
@Getter
@Setter
@NoArgsConstructor(access = PROTECTED)
@AllArgsConstructor(access = PROTECTED)
public abstract class AuditableBase implements Serializable {

	private static final long serialVersionUID = 1L;

	@Embedded
	private AuditMetadata metadadoAuditoria = new AuditMetadata();
}
