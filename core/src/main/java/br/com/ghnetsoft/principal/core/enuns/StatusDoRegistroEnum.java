package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum StatusDoRegistroEnum {

	ATIVO("Ativo", "/imagens/stock_lock_open.png"), BLOQUEADO("Bloqueado", "/imagens/stock_lock.png"),
	INATIVO("Inativo", "/imagens/stock_lock.png"), EXCLUIDO("Excluído", "/imagens/stock_lock.png");

	private String descricao;
	private String imagem;

	StatusDoRegistroEnum(String descricao, String imagem) {
		this.descricao = descricao;
		this.imagem = imagem;
	}

	/**
	 * busca o StatusDoRegistroEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static StatusDoRegistroEnum buscaStatusDoRegistroEnum(String descricao) {
		for (StatusDoRegistroEnum enun : StatusDoRegistroEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
