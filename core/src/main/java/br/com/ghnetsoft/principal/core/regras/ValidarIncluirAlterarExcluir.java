package br.com.ghnetsoft.principal.core.regras;

import java.util.Collection;

import br.com.ghnetsoft.principal.core.enuns.StatusDoRegistroEnum;
import br.com.ghnetsoft.principal.core.modelo.ProPrincipal;
import br.com.ghnetsoft.principal.utilitario.mensagem.Mensagem;

/**
 * @author Guilherme C Lopes
 */
public interface ValidarIncluirAlterarExcluir {

	/**
	 * validação de resouce ao incluir
	 * 
	 * @param id
	 * @param mensagens
	 */
	void validarIncluir(Long id, Collection<Mensagem> mensagens);

	/**
	 * validação de resouce ao alterar
	 * 
	 * @param id
	 * @param mensagens
	 */
	void validarAlterar(Long id, Collection<Mensagem> mensagens);

	/**
	 * validação de resouce ao alterar com ID diferente
	 * 
	 * @param objeto
	 * @param mensagens
	 */
	void validarAlterarRegistroIdnulo(ProPrincipal objeto, Collection<Mensagem> mensagens);

	/**
	 * validação de resouce ao excluir
	 * 
	 * @param id
	 * @param mensagens
	 */
	void validarExcluir(Long id, Collection<Mensagem> mensagens);

	/**
	 * validação de resouce se ele já está excluido
	 * 
	 * @param status
	 * @param mensagens
	 * @param chave
	 * @param entidade
	 */
	void validarExcluirGeral(StatusDoRegistroEnum status, Collection<Mensagem> mensagens, String chave,
			String entidade);
}
