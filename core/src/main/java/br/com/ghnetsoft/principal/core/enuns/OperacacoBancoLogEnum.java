package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum OperacacoBancoLogEnum {

	INCLUSAO("Inclusão"), ALTERACAO("Alteração"), EXCLUSAO("Exclusão");

	private String descricao;

	OperacacoBancoLogEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o OperacacoBancoLogEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static OperacacoBancoLogEnum buscaOperacacoBancoLogEnum(String descricao) {
		for (OperacacoBancoLogEnum enun : OperacacoBancoLogEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
