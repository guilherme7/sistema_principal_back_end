package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum PessoaTipoEnum {

	FISICA("Física"), JURIDICA("Jurídica");

	private String descricao;

	PessoaTipoEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o PessoaTipoEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static PessoaTipoEnum buscaPessoaTipoEnum(String descricao) {
		for (PessoaTipoEnum enun : PessoaTipoEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
