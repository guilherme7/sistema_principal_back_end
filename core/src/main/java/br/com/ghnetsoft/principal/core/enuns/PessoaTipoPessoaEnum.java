package br.com.ghnetsoft.principal.core.enuns;

import lombok.Getter;

/**
 * @author Guilherme C Lopes
 */
@Getter
public enum PessoaTipoPessoaEnum {

	CLIENTE("Cliente"), FORNECEDOR("Fornecedor"), FUNCIONARIO("Funcionário"), TRANSPORTADOR("Transportador"),
	FABRICANTE("Fabricante"), SEGURADORA("Seguradora"), CORRETOR("Corretor"), TOMADOR_SERVICO("Tomador de Serviço"),
	SINDICATO("Sindicato"), PROTOCOLO("Protocolo"), REPRESENTANTE("Representante"),
	FUNDO_INVESTIMENTO_PJ("Fundo de Investimento PJ"), ESCRITORIO_ADVOGADO("Escritório de Advogado"),
	PLANO_SAUDE("Plano de Saúde"), CONTABILIDADE("Contabilidade"), CONTRIBUINTE("Contribuinte"), USUARIO("Usuário");

	private String descricao;

	PessoaTipoPessoaEnum(String descricao) {
		this.descricao = descricao;
	}

	/**
	 * busca o PessoaTipoPessoaEnum pela descrição
	 * 
	 * @param descricao
	 * @return
	 */
	public static PessoaTipoPessoaEnum buscaPessoaTipoPessoaEnum(String descricao) {
		for (PessoaTipoPessoaEnum enun : PessoaTipoPessoaEnum.values()) {
			if (enun.name().equals(descricao)) {
				return enun;
			}
		}
		return null;
	}
}
